import Vue from 'vue'
import VueRouter from 'vue-router'
import Navbar from '@/components/Navbar'

describe('Navbar.vue', () => {
  it('should render correct contents', () => {
    const vm = new Vue({
      el: document.createElement('div'),
      render: (h) => h(Navbar)
    })
    expect(vm.$el.querySelector('.nav-title').textContent)
      .toBe('GitLab Training Library')
  });

 it('renders to a nice snapshot', () => {
    const renderer = require('vue-server-renderer').createRenderer();
    const vm = new Vue({
      el: document.createElement('div'),
      render: h => h(Navbar),
    });
    renderer.renderToString(vm, (err, str) => {
      expect(str).toMatchSnapshot();
    });
  });
})